package test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.RingList;

public class RingListTest extends TestCase
{
	private RingList rl;

	public void setUp()
	{
		rl = new RingList();
		rl.add(new Integer(1));
		rl.add(new Integer(54));
		rl.add(new Integer(35));
		rl.add(new Integer(12));
		rl.add(new Integer(50));
	}
	
	public void testGiveFirstNode()
	{
		assertEquals(new Integer(1), rl.giveFirstNode());
	}
	
	public void testIterator()
	{
		Iterator a = rl.iterator();
		while(a.hasNext())
		{
			a = (Iterator) a.next();
		}
		assertEquals("No se iter� correctamente", null, a.next());
	}
	
	public void testGetSize()
	{
		assertEquals(new Integer(4), rl.getSize());
	}
	
	public void testAddAtEnd()
	{
		assertEquals(new Integer(5), rl.getElement(5));
	}
	
	public void testAddAtK()
	{
		rl.addAtK(10, 3);
		assertEquals(new Integer(10),rl.getElement(10));
	}
	
	public void testGetElement()
	{
		assertEquals(new Integer(54), rl.getElement(1));
	}
	
	public void testDelete()
	{
		rl.delete(50);
		assertEquals(null, rl.getElement(4));
	}
	
	public void testNext()
	{
		assertEquals(true, rl.next());
	}
	
	public void testPrevious()
	{
		assertEquals(true, rl.previous());
	}
	
	public void testAdd()
	{
		rl.add(17);
		assertEquals(new Integer(17), rl.getElement(5));
	}
	
	public void testDeleteAtK()
	{
		rl.deleteAtK(1, 0);
		assertEquals(null, rl.getElement(0));
	}
}
