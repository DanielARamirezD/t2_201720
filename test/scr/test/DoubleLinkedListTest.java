package test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;

public class DoubleLinkedListTest extends TestCase
{
	private DoubleLinkedList dll;
	
	public void setUp()
	{
		dll = new DoubleLinkedList();
		dll.add(new Integer(1));
		dll.add(new Integer(54));
		dll.add(new Integer(35));
		dll.add(new Integer(12));
		dll.add(new Integer(50));	}
	
	public void testIterator()
	{
		Iterator a = dll.iterator();
		while(a.hasNext())
		{
			a = (Iterator) a.next();
		}
		assertEquals("No se iter� correctamente", null, a.next());
	}
	
	public void testGetSize()
	{
		assertEquals(new Integer(4), dll.getSize());
	}
	
	public void testAddAtEnd()
	{
		assertEquals(new Integer(5), dll.getElement(5));
	}
	
	public void testAddAtK()
	{
		dll.addAtK(10, 3);
		assertEquals(new Integer(10), dll.getElement(10));
	}
	
	public void testGetElement()
	{
		assertEquals(new Integer(54), dll.getElement(1));
	}
	
	public void testDelete()
	{
		dll.delete(1);
		assertEquals(null, dll.getElement(0));
	}
	
	public void testNext()
	{
		assertEquals(true, dll.next());
	}
	
	public void testPrevious()
	{
		assertEquals(true, dll.previous());
	}
	
	public void testAdd()
	{
		dll.add(17);
		assertEquals(new Integer(17), dll.getElement(5));
	}
	
	public void testDeleteAtK()
	{
		dll.deleteAtK(1, 0);
		assertEquals(null, dll.getElement(0));
	}
	
}
