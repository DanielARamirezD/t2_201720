package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;

public class STSManager implements ISTSManager {

	private VORoute r;
	private VOTrip t;
	private VOStopTime st;
	private VOStop s;
	private DoubleLinkedList<VORoute> lr;
	private DoubleLinkedList<VOTrip> lt;
	private DoubleLinkedList<VOStopTime> lst;
	private DoubleLinkedList<VOStop> ls;
	

	public void loadRoutes(String routesFile) 
	{
		File file = new File(routesFile);
		
		try 
		{
			BufferedReader ln = new BufferedReader(new FileReader(file));
			String linea = ln.readLine();
			lr = new DoubleLinkedList<VORoute>(); 
			
			while(linea != null)
			{
				String[] datos = linea.split(",");
				String routeID = datos[0];
				String agencyID = datos[1];
				String routeShortName = datos[2];
				String routeLongName = datos[3];
				String routeDesc = datos[4];
				String routeType = datos[5];
				String routeURL = datos[6];
				String routeColor = datos[7];
				String routeTextColor = datos[8];
				r = new VORoute(routeID, agencyID, routeShortName, routeLongName, routeDesc, routeType, routeURL, routeColor, routeTextColor);
				lr.add(r);
				linea = ln.readLine();
			}
			ln.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
	}
	
	public void loadTrips(String tripsFile) 
	{
		File file = new File(tripsFile);
		
		try 
		{
			BufferedReader ln = new BufferedReader(new FileReader(file));
			String linea = ln.readLine();
			lt = new DoubleLinkedList<VOTrip>();
			
			while(linea != null)
			{
				String[] datos = linea.split(",");
				String routeID = datos[0];
				String serviceID = datos[1];
				String tripID = datos[2];
				String tripHeadSign = datos[3];
				String tripShortName = datos[4];
				String directionID = datos[5];
				String blockID = datos[6];
				String shapeID = datos[7];
				String wheelchairAccesible = datos[8];
				String bikesAllowed = datos[9];
				t = new VOTrip(routeID, serviceID, tripID, tripHeadSign, tripShortName, directionID, blockID, shapeID, wheelchairAccesible, bikesAllowed);
				lt.add(t);
				linea = ln.readLine();
			}
			ln.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	

	public void loadStopTimes(String stopTimesFile) 
	{
		File file = new File(stopTimesFile);
		
		try 
		{
			BufferedReader ln = new BufferedReader(new FileReader(file));
			String linea = ln.readLine();
			lst = new DoubleLinkedList<VOStopTime>();
			
			while(linea != null)
			{
//				
				String[] datos = linea.split(",");
				String tripID = datos[0];
				String arrivalTime = datos[1];
				String departureTime = datos[2];
				String stopID = datos[3];
				String stopSequence = datos[4];
				String stopHeadsign = datos[5];
				String pickupType = datos[6];
				String dropOffType = datos[7];
				String shapeDistTraveled = datos[7];
				
				
				System.out.println(datos.length);
				st = new VOStopTime(tripID, arrivalTime, departureTime, stopID, stopSequence, stopHeadsign, pickupType, dropOffType, shapeDistTraveled);
				lst.add(st);
				linea = ln.readLine();
			}
			ln.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public void loadStops(String stopsFile) 
	{
		File file = new File(stopsFile);
		
		try 
		{
			BufferedReader ln = new BufferedReader(new FileReader(file));
			String linea = ln.readLine();
			ls = new DoubleLinkedList<VOStop>();
			
			while(linea != null)
			{
				String[] datos = linea.split(",");
				String stopID = datos[0];
				String stopCode = datos[1];
				String stopName = datos[2];
				String stopDesc = datos[3];
				String stopLat = datos[4];
				String stopLon = datos[5];
				String zoneID = datos[6];
				String stopURL = datos[7];
				String locationType = datos[8];
				String parentStation = datos[9];
				s = new VOStop(stopID, stopCode, stopName, stopDesc, stopLat, stopLon, zoneID, stopURL, locationType, parentStation);
				ls.add(s);
				linea = ln.readLine();
			}
			ln.close();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}

	public IList<VORoute> routeAtStop(String stopName) 
	{
		DoubleLinkedList<VORoute> route = new DoubleLinkedList<VORoute>();
		
		Iterator<VOStop> iter = ls.iterator();
		
		while(iter.hasNext())
		{
			VOStop busStop = (VOStop) iter.next();
			
			if (busStop.getName().compareTo(stopName) == 0)
			{
				route.getElement(busStop.id()).getName();
			}
		}
		return route;
	}

	public IList<VOStop> stopsRoute(String routeName, String direction) 
	{
		IList<VOStop> sr = new DoubleLinkedList<VOStop>();
		Iterator a = lr.iterator();
		while(a.hasNext())
		{
			VOStop temp = (VOStop) a.next();
			if(temp.getName().equals(routeName))
			{
				Iterator b = lt.iterator();
				while(b.hasNext())
				{
					VOTrip temp1 = (VOTrip) b.next();
					if(temp1.rID() == temp.id())
					{
						if(temp1.getDirection().equals(direction))
							sr.add(temp);
					}
				}
			}
		}
		
		return sr;
	}

}
