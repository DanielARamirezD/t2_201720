package model.vo;

/**
 * Representation of a Stop object
 */
public class VOStop implements Comparable<VOStop>
{
	
	private String stop_id;
	private String stop_code;
	private String stop_name;
	private String stop_desc;
	private String stop_lat;
	private String stop_lon;
	private String zone_id;
	private String stop_url;
	private String location_type;
	private String parent_station;
	
	public VOStop(String pStopID, String pSC, String pSN, String pSD, String pSLa, String pSLo, String pZoneID, String pSURL, String pLT, String pPs)
	{
		stop_id = pStopID;
		stop_code = pSC;
		stop_name = pSN;
		stop_desc = pSD;
		stop_lat = pSLa;
		stop_lon = pSLo;
		zone_id = pZoneID;
		stop_url = pSURL;
		location_type = pLT;
		parent_station = pPs;
	}

	/**
	 * @return id - stop's id
	 */
	public int id() 
	{
		return Integer.parseInt(stop_id);
	}

	/**
	 * @return name - stop name
	 */
	public String getName() 
	{
		return stop_name;
	}
	
	public int compareTo(VOStop o)
	{
		if(this.id() == o.id())
		{
			return 0;
		}
		else if(this.id() < o.id())
		{
			return -1;
		}
		else
			return 1;
	}

}
