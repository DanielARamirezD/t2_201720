package model.vo;

public class VOTrip implements Comparable<VOTrip>
{
	private String route_id;
	private String service_id;
	private String trip_id;
	private String trip_headsign;
	private String trip_short_name;
	private String direction_id;
	private String block_id;
	private String shape_id;
	private String wheelchair_accessible;
	private String bikes_allowed;
	
	public VOTrip(String pRouteID, String pServiceID, String pTripID, String pTripHS, String pTripSN, String pDirectionID, String pBlockID, String pShapeID, String pWA, String pBA)
	{
		route_id = pRouteID;
		service_id = pServiceID;
		trip_id = pTripID;
		trip_headsign = pTripHS;
		trip_short_name = pTripSN;
		direction_id = pDirectionID;
		block_id = pBlockID;
		shape_id = pShapeID;
		wheelchair_accessible = pWA;
		bikes_allowed = pBA;
	}
	
	public int id() 
	{
		return Integer.parseInt(trip_id);
	}
	
	public int rID()
	{
		return Integer.parseInt(route_id);
	}
	
	public String getDirection() 
	{
		return direction_id;
	}

	
	public int compareTo(VOTrip o) 
	{
		if(this.id() == o.id())
		{
			return 0;
		}
		else if(this.id() < o.id())
		{
			return -1;
		}
		else
			return 1;
	}

}
