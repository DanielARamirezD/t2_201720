package model.vo;

public class VOStopTime implements Comparable<VOStopTime>
{
	private String trip_id;
	private String arrival_time;
	private String departure_time;
	private String stop_id;
	private String stop_sequence;
	private String stop_headsign;
	private String pickup_type;
	private String drop_off_type;
	private String shape_dist_traveled;
	
	public VOStopTime(String pTripID, String pAT, String pDT, String pStopID, String pSS, String pSHS, String pPT, String pDOT, String pSDT)
	{
		trip_id = pTripID;
		arrival_time = pAT;
		departure_time = pDT;
		stop_id = pStopID;
		stop_sequence = pSS;
		stop_headsign = pSHS;
		pickup_type = pPT;
		drop_off_type = pDOT;
		shape_dist_traveled = pSDT;
	}
	
	public int id() 
	{
		return Integer.parseInt(trip_id);
	}

	public int compareTo(VOStopTime o) 
	{
		if(this.id() == o.id())
		{
			return 0;
		}
		else if(this.id() < o.id())
		{
			return -1;
		}
		else
			return 1;
	}
}
