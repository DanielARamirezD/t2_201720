package model.vo;

/**
 * Representation of a route object
 */
public class VORoute implements Comparable<VORoute>
{

	private String route_id;
	private String agency_id;
	private String route_short_name;
	private String route_long_name;
	private String route_desc;
	private String route_type;
	private String route_url;
	private String route_color;
	private String route_text_color;
	
	public VORoute(String pRouteID, String pAgencyID, String pRouteShort, String pRouteLong, String pDesc, String pType, String pUrl, String pColor, String pText)
	{
		route_id = pRouteID;
		agency_id = pAgencyID;
		route_short_name = pRouteShort;
		route_long_name = pRouteLong;
		route_desc = pDesc;
		route_type = pType;
		route_url = pUrl;
		route_color = pColor;
		route_text_color = pText;
	}
	
	
	/**
	 * @return id - Route's id number
	 */
	public int id() 
	{
		return Integer.parseInt(route_id);
	}

	/**
	 * @return name - route name
	 */
	public String getName() 
	{
		return route_short_name;
	}


	public int compareTo(VORoute o) 
	{
		if(this.id() == o.id())
		{
			return 0;
		}
		else if(this.id() < o.id())
		{
			return -1;
		}
		else
			return 1;
	}

}
