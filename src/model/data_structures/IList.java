package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	Integer getSize();
	
	void add(T elem) ;
	
	void addAtEnd(T elem);
	
	void addAtK(T elem, int k);
	
	T getElement(int idNode);
	
	void delete(T elem);
	
	void deleteAtK(T elem, int k);
	
	boolean next();
	
	boolean previous();

}
